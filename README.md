# Magnolia translation merge tool

Use this tool to merge a magnolia csv export with the local properties files.

1. Copy your magnolia csv export to: `magnolia-export`
2. Copy your properties files to: `magnolia-export`
3. Start npm task `npm start`
4. You can find your csv file in the `export` folder

### Notes:
* The translations will be sorted by the key
* The translations in the magnolia-export folder overrule the properties translations
* Duplicated keys and empty values will be removed from the output
* Translations at the end of a file will overrule the translations at the beginning