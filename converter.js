const fs = require('fs');

const outputFile = './export/translations.csv';
const inputMagnolia = './magnolia-export/';
const inputProperties = './properties-files/';
const excludedFiles = [
    '.gitkeep',
];

const translations = {};

const addTranslation = (translation, splitIcon = ',') => {
    const parts = translation.split(splitIcon);
    const key = parts[0].trim();
    if (parts.length <= 1 || translations[key] !== undefined) return;
    translations[key] = parts.slice(1).reduce((a, b) => a + (a && b ? splitIcon : '') +  b, '');
};

const readFolder = (folder) => {
    const files = fs.readdirSync(folder);

    files.forEach(file => {
        if (excludedFiles.includes(file)) return;

        const data = fs.readFileSync(`${folder}${file}`, {
            encoding: 'utf8',
        });

        if (!data) {
            console.error(`Invalid file: ${file}`);
            return;
        }

        const lines = data.split('\n');
        let splitIcon = ','; // default is csv

        if (file.endsWith('.properties')) {
            splitIcon = '=';
        }

        lines.reverse().forEach(line => addTranslation(line, splitIcon));
    });
};

readFolder(inputMagnolia);
readFolder(inputProperties);

let output = '';
const entries = Object.entries(translations);

// Sort by key
entries.sort((a, b) => {
    if (a[0] < b[0]) {
        return -1;
    } else if (a[0] > b[0]) {
        return 1;
    }

    return 0;
});

// Save all entries in output string;
entries.forEach(([key, translationRaw], index) => {
    const translation = translationRaw.trim();
    if (key !== 'key' && translation !== '') {
        output += `${key},"${translation.split('"').join('""')}"\n`;
    }
});

// Save file
fs.writeFileSync(outputFile, output, {
    encoding: 'utf8',
});
